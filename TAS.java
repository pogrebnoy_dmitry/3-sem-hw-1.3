package org.sample;

import java.util.concurrent.atomic.AtomicBoolean;

public class TAS implements Lock {
    AtomicBoolean flag = new AtomicBoolean(false);
    public void lock() {
        while (flag.getAndSet(true)){}
    }
    public void unlock() {
        flag.set(false);
    }
}
