package org.sample;

public class AddOne implements Runnable {
    private Counter counter;
    private Lock lock;

    public AddOne(Counter counter, Lock lock) {
        this.counter = counter;
        this.lock = lock;
    }

    @Override
    public void run() {
        for (int i = 0; i < 500000; i++) {
            lock.lock();
            counter.incOneCounter();
            lock.unlock();
        }
    }
}