package org.sample;

import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

public class MyBenchmark {
    @State(Scope.Thread)
    public static class MyState {
        @Setup(Level.Trial)
        public void doSetup() {
            adder = new AddOne[4];
            counter = new Counter(0);
        }
        @TearDown(Level.Trial)
        public void doTearDown() {}
        AddOne[] adder;
        Counter counter;
    }
    @Benchmark @Fork(1) @Warmup(iterations = 3) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void testTAS(MyState state) {
        Lock lock = new TAS();
        for (int i = 0; i < 4; i++) {
            state.adder[i] = new AddOne(state.counter, lock);
            Thread thread = new Thread(state.adder[i]);
            thread.start();
        }
    }
    @Benchmark @Fork(1) @Warmup(iterations = 3) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void testTTAS(MyState state) {
        Lock lock = new TTAS();
        for (int i = 0; i < 4; i++) {
            state.adder[i] = new AddOne(state.counter, lock);
            Thread thread = new Thread(state.adder[i]);
            thread.start();
        }
    }
}