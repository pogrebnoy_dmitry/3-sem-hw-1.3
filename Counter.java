package org.sample;

public class Counter {
    private int counter;

    public Counter(int counter) {
        counter = counter;
    }

    public void incOneCounter() {
        counter++;
    }

    public int getCounter() {
        return counter;
    }
}
