package org.sample;

import java.util.concurrent.atomic.AtomicBoolean;

public class TTAS implements Lock {
    protected AtomicBoolean exlock;
    public TTAS() {
        exlock = new AtomicBoolean(false);
    }
    public void lock() {
        boolean expoint = false;

        while(!expoint) {
            if(!exlock.get()) {
                expoint = exlock.compareAndSet(false, true);
            }
        }
    }
    public boolean tryLock() {
        if(exlock.get()) {
            return false;
        }
        return exlock.compareAndSet(false, true);
    }
    public void unlock() {
        exlock.set(false);
    }
}