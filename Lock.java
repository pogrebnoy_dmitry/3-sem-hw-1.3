package org.sample;

public interface Lock {
    void lock();
    void unlock();
}
