package org.sample;

public class Main {
    public static void main(String[] args) {
        Counter count = new Counter(0);
        Lock lock = new TAS();
        AddOne[] adder = new AddOne[4];

        for (int i = 0; i < 4; i++) {
            adder[i] = new AddOne(count, lock);
            Thread thread = new Thread(adder[i]);
            thread.start();
        }
    }
}
